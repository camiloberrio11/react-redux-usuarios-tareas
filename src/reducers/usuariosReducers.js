import { TRAER_USUARIOS, CARGANDO, ERROR } from '../types/usuariosTypes';

const INITIAL_STATE = {
  usuarios: [],
  cargando: false,
  error: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TRAER_USUARIOS:
      return { ...state, usuarios: action.payload, cargando: false };
    case CARGANDO:
      return { ...state, cargando: action.payload };
    case ERROR:
      return { ...state, cargando: false, error: action.payload };
    default:
      return { ...state };
  }
};
