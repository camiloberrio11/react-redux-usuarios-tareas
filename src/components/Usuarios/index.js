import React, { Component } from 'react';
import { connect } from 'react-redux';
import Spinner from '../shared/Spinner';
import * as usuariosActions from '../../actions/usuarioActions';
import ErrorFatal from '../shared/ErrorFatal';
import Tabla from './Tabla';

class Usuarios extends Component {
  componentDidMount() {
    // Funcion de montado
    this.props.traerTodosUsuarios();
  }

  ponerContenido = () => {
    if (this.props.cargando) {
      return <Spinner />;
    }
    if (this.props.error) {
      return <ErrorFatal errorMsg={this.props.error} />;
    }
    return <Tabla />;
  };

  render() {
    return (
      <div>
        <h1>Usuarios</h1>
        {this.ponerContenido()}
      </div>
    );
  }
}

const mapStateToProps = (reducers) => {
  // Reducers que yo quiero
  return reducers.usuariosReducer;
};

export default connect(mapStateToProps, usuariosActions)(Usuarios);
