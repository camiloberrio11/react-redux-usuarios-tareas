import React from 'react';

const ErrorFatal = ({ errorMsg }) => {
  return (
    <div className="center rojo">
      <h2>Algo salió mal</h2>
      <p>{errorMsg}</p>
      <p><b>Intenta más tarde</b></p>
    </div>
  );
};

export default ErrorFatal;
