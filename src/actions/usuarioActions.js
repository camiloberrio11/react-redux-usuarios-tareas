import axios from 'axios';
import { CARGANDO, ERROR, TRAER_USUARIOS } from '../types/usuariosTypes';

export const traerTodosUsuarios = () => async (dispatch) => {
  dispatch(cargandoDatos());
  try {
    const respuesta = await axios.get(
      'https://jsonplaceholder.typicode.com/users'
    );
    dispatch(obtenerUsuariosExitoso(respuesta.data));
  } catch (error) {
    dispatch(obtenerUsuariosError(error));
  }
};

const obtenerUsuariosExitoso = (data) => ({
  type: TRAER_USUARIOS,
  payload: data,
});

const obtenerUsuariosError = (error) => ({
  type: ERROR,
  payload: error.message,
});

const cargandoDatos = () => ({
  type: CARGANDO,
  payload: true,
});
